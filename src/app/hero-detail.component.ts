import { Component, Input  }   from '@angular/core';
import { Hero       }   from './hero';

@Component({
    selector:   'app-hero-detail',
    template:   `
    <div *ngIf="heroDetail">
    <h2>{{heroDetail.name}} details!</h2>
    <div><label>id: </label>{{heroDetail.id}}</div>
    <div>
      <label>name: </label>
      <input [(ngModel)] ="heroDetail.name" placeholder="name" >
    </div>
  </div>`,
})

export class HeroDetailComponent {
    @Input() heroDetail: Hero;
}
